"use strict";

var ChannelModule = require("./module");
var Flags = require("../flags");

function makeIPHash(ip) {
	var numeric = 0;
	for(var i = 0 ; i < ip.length ; i++){
		if(ip.charAt(i) != '.'){
			numeric += ip.charAt(i);
		}
	}
	numeric = parseInt(numeric);
	return  makeID((numeric * 13) % 1000000);
}

function makeID(id_number){
	var id_string = "" + id_number;
	var processed_string = "";
	for(var i = 0 ; i < id_string.length ; i += 2){
		var num = id_string.charAt(i)
		if(i + 1 < id_string.length) num += id_string.charAt(i + 1);
		num = parseInt(num) % 61;
		processed_string += convertNumToIDChar("" + num);
	}
	return processed_string;
}

// Xaekai on Jan 14, 2019
function convertNumToIDChar(num){
	const i = parseInt(num, 10);
	switch(true){
		case i > -1 && i < 26: return String.fromCharCode(i+97);
		case i > 25 && i < 52: return String.fromCharCode(i+39);
		case i > 51 && i < 61: return String.fromCharCode(i-3);
		case i === 61: return '0';
	}
	console.log('notret');
}

function AnonymousPost(_channel) {
    ChannelModule.apply(this, arguments);
}

AnonymousPost.prototype = Object.create(ChannelModule.prototype);

AnonymousPost.prototype.onUserPreJoin = function (user, data, cb) {
    const opts = this.channel.modules.options;
    var anonymousPosting = opts.get("allow_anon_chat");
	if(anonymousPosting && user.isAnonymous()){
		var hash = makeIPHash(user.realip);
		var anon_name = "Anonymous-" + hash;
		var duplicate_solution = 0;
		var duplicate_found = false;
		do{
			duplicate_found = false;
			user.channel.users.forEach((user, i) => {
				if (!duplicate_found && user.account.name == anon_name){
					duplicate_solution++;
					anon_name = "Anonymous-" + hash + duplicate_solution;
					duplicate_found = true;
				}
			});
		}
		while(duplicate_found);
		user.guestLogin(anon_name);
		cb(null, ChannelModule.PASSTHROUGH);
	}
	else{
		cb(null, ChannelModule.PASSTHROUGH);
	}
};

module.exports = AnonymousPost;
//# sourceMappingURL=anonymouspost.js.map